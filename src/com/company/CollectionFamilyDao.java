package com.company;

import com.company.*;
import java.util.*;

public class CollectionFamilyDao implements FamilyDao{
    private final List<Family> families = new ArrayList<Family>();

    @Override
    public List<Family> getAllFamilies() {
        return families;
    }

    @Override
    public Family getFamilyByIndex(int id) {
        if(id > families.size())    return null;
        return families.get(id);
    }

    @Override
    public boolean deleteFamily(int id) {
        if(id > families.size())    return false;
        families.remove(id);
        return true;
    }

    @Override
    public boolean deleteFamily(Family family) {
        if(!families.contains(family))    return false;
        families.remove(family);
        return true;
    }

    @Override
    public void saveFamily(Family family) {
        this.families.add(family);
    }
}