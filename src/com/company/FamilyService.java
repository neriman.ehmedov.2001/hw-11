package com.company;

import com.company.*;
import com.company.Pet;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class FamilyService {
    private final FamilyDao familyDao = new CollectionFamilyDao();

    public void saveFamily(Family family){
        familyDao.saveFamily(family);
    }
    public List<Family> getAllFamilies(){
        return familyDao.getAllFamilies();
    }
    public void createNewFamily(Human father, Human mother){
        Family newFamily = new Family(mother, father);
        familyDao.saveFamily(newFamily);
    }
    public void deleteFamilyByIndex(int index){
        familyDao.deleteFamily(index);
    }
    public void bornChild(int index, Human child){
        familyDao.getFamilyByIndex(index).addChild(child);
    }
    public void adoptChild(int index, ArrayList<Human> child){
        familyDao.getFamilyByIndex(index).setChildren(child);
    }
    public int count(){
        return familyDao.getAllFamilies().size();
    }
    public Family getFamilyById(int index){
        return familyDao.getFamilyByIndex(index);
    }
    public Pet getPets(){
        List<Family> familyList = familyDao.getAllFamilies();
        //List<Objects> petList = new List<Dog>();
        Pet pet = new Dog();
        for(Family i : familyList) {
            pet = i.getPet();
        }
        return pet;
    }
    public void addPet(int index, Pet pet){
        familyDao.getFamilyByIndex(index).setPet(pet);
    }
    public void countFamiliesWithMemberNumber(int count){
        List<Family> familyList = familyDao.getAllFamilies();
        final int[] index = {0};
        familyList.forEach(new Consumer<Family>() {

            public void accept(Family t) {
                if (count == t.countFamily())
                    index[0]++;
            }

        });
        System.out.println(index[0]);
    }


    public void displayAllFamilies(){
        List<Family> familyList = familyDao.getAllFamilies();
        final int[] index = {0};
        familyList.forEach(new Consumer<Family>() {
            public void accept(Family t) {
                System.out.println("Index: " + index[0] + "\t" + "Family: " + t.toString());
                index[0]++;
            }
        });
    }//1
    public void getFamiliesBiggerThan(int count){
        List<Family> familyList = familyDao.getAllFamilies();
        Stream<Family> parallelStream = familyList.parallelStream();
        Stream<Family> bigger = parallelStream.filter(p -> p.countFamily() > count);
        System.out.println("Bigger families: ");
        bigger.forEach(p -> System.out.println(p.toString()));
    }//2
    public void getFamiliesLessThan(int count){
        List<Family> familyList = familyDao.getAllFamilies();
        Stream<Family> parallelStream = familyList.parallelStream();
        Stream<Family> smaller = parallelStream.filter(p -> p.countFamily() < count);
        System.out.println("Smaller families: ");
        smaller.forEach(p -> System.out.println(p.toString()));
    }//3
    public void deleteAllChildrenOlderThen(int index, Human child){
        familyDao.getFamilyByIndex(index).deleteChild(child);
    }//5
}
